var mymap = L.map('mapid').setView([-32.308450, -58.067972], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
}).addTo(mymap);

$.ajax({
  dataType: 'json',
  url: "api/bicicletas",
  success: function (result) {
    console.log(result);
    result.bicicletas.forEach(function (bici) {
      var marker = L.marker(bici.ubicacion, { title: bici.id }).addTo(mymap);
    });
  }
})


/*
var marker = L.marker([-32.308450, -58.067972]).addTo(mymap);
marker.bindPopup("<b>TRAZA!</b><br>Ubicacion Traza.").openPopup();

var circle = L.circle([-32.3211068, -58.0705143], {
  color: 'red',
  fillColor: '#f03',
  fillOpacity: 0.5,
  radius: 500
}).addTo(mymap);
circle.bindPopup("Rango de trabajo");

var polygon = L.polygon([
  [-32.3120447, -58.0898512],
  [-32.3182647, -58.0867399],
  [-32.309678, -58.0863536]
]).addTo(mymap);
polygon.bindPopup("I am a polygon.");

*/